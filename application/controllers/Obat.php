<?php

class Obat extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_Obat');
        $this->load->model('Model_JenisObat');
    }
    
    public function index()
    {
        $data['Obat'] = $this->Model_Obat->getAllObat();
        $this->load->view('template/header');
		$this->load->view('ManageObat', $data);
        $this->load->view('template/footer');
    }

	public function pageAdd()
	{
        $data['JenisObat'] = $this->Model_JenisObat->getAllJenisObat();
        $this->load->view('template/header');
		$this->load->view('TambahObat', $data);
        $this->load->view('template/footer');
	}
    
    public function tambahData()
    {
        $this->Model_Obat->tambahData();
        redirect('Obat');
    }

    public function pageEdit($id_obat )
    {
        
        $data['Obat'] = $this->Model_Obat->getObatById($id_obat);
        $data['JenisObat'] = $this->Model_JenisObat->getAllJenisObat();
        $this->load->view('template/header');
		$this->load->view('EditObat', $data);
        $this->load->view('template/footer');
    }

    public function editData()
    {
        $this->Model_Obat->update();
        redirect('Obat');
    }

    public function delete($id_obat)
    {
        $this->Model_Obat->hapusData($id_obat);
        redirect('Obat');
    }
}

?>