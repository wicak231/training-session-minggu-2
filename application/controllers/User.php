<?php

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_User');
        $this->load->model('Model_JenisObat');
        $this->load->model('Model_Obat');

    }
    
    public function index()
    {
        $data['User'] = $this->Model_User->getAllUser();
        $this->load->view('template/header');
		$this->load->view('ManageUser', $data);
        $this->load->view('template/footer');
    }

    public function pageReg()
    {
        $this->load->view('register');
    }

    public function register()
    {
        $this->Model_User->register();
        redirect('Home/index');
    }

    public function pageEdit($id_user )
    {
        
        $data['User'] = $this->Model_User->getUserById($id_user);
        $this->load->view('template/header');
		$this->load->view('editUser', $data);
        $this->load->view('template/footer');
    }

    public function editData()
    {
        $this->Model_User->update();
        redirect('User');
    }
    public function delete($id_user)
    {
        $this->Model_User->hapusData($id_user);
        redirect('User');
    }

    public function loginProses()
    {
      $username = $this->input->post('username');
      $password = $this->input->post('password');

      $user = $this->db->get_where('tb_user', ['username' => $username])->row_array();
     if ($user) {
        if ($user['is_active'] == 'on') {
           if ($user['password'] == $password) {
            if ($user['level'] == 'admin') {
                $data['Obat'] = $this->Model_Obat->getAllObat();
                $data['JenisObat'] = $this->Model_JenisObat->getAllJenisObat();
                $this->load->view('template/header');
                $this->load->view('AdminPage', $data);
                $this->load->view('template/footer');
            }else{
                $this->load->view('HalamanUtama');
            }
           } else{
            echo "Gagal Password Salah";
           }
        }else{
            echo "Gagal User Belum Aktif";
        }
     } else {}
    }

    public function logout()
    {
        $this->session->unset_userdata['username'];
        $this->session->unset_userdata['fullname'];
        return redirect('Home/index');
    }
}

?>