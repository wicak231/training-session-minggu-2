<?php

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Model_Obat');
		$this->load->model('Model_JenisObat');

	}
	
	
	public function index()
	{
		$data['Obat'] = $this->Model_Obat->getAllObat();
        $data['JenisObat'] = $this->Model_JenisObat->getAllJenisObat();
		// $data['Jumlah'] = $this->Model_Obat->Jumlah();
		$data['total'] = $this->Model_Obat->hitungJumlahAsset();
        $this->load->view('template/header');
		$this->load->view('AdminPage', $data);
        $this->load->view('template/footer');
		// print_r($datajenis);
	}

	public function search()
	{
		$keyword = $this->input->post('keyword');
		$data['Obat'] = $this->Model_Obat->get_keyword($keyword);
		$data['JenisObat'] = $this->Model_JenisObat->getAllJenisObat();
		$this->load->view('template/header');
		$this->load->view('AdminPage', $data);
        $this->load->view('template/footer');
		// print_r($data);
	}

	public function cari()
	{
		$keyword = $this->input->post('cari');
		$data['Obat'] = $this->Model_Obat->get_jenis($keyword);
		$data['JenisObat'] = $this->Model_JenisObat->getAllJenisObat();
		$this->load->view('template/header');
		$this->load->view('AdminPage', $data);
        $this->load->view('template/footer');
		// print_r($keyword);
	}

	public function tanggal () {
		$tawal = $this->input->post('tawal');
		$takhir = $this->input->post('takhir');
		$data['Obat'] = $this->Model_Obat->rentang($tawal, $takhir);
		$this->load->view('template/header');
		$this->load->view('AdminPage', $data);
        $this->load->view('template/footer');
		// print_r($data);
	}


	public function cetakPDF()
	{
		$this->load->library('dompdf_gen');
		$data['Obat'] = $this->Model_Obat->getAllObat();
		$this->load->view('laporan_pdf', $data);

		$paper_size = 'A4';
		$orientation = 'portait';
		$html = $this->output->get_output();
		$this->dompdf->set_paper($paper_size, $orientation);

		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("Laporan_Obat.pdf", array('Attachment' =>0));
	}

	public function cetakExcel()
	{
		$data['Obat'] = $this->Model_Obat->getAllObat();

		require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel.php');
		require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

		$object = new PHPExcel();

		$object->getProperties()->setCreator("Techno Medic")
		->setLastModifiedBy("Techno Medic")
		->setTitle("Daftar Data Obat")
		->setSubject('Office 2007 XLSX Test Document')
		->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
		->setKeywords('office 2007 openxml php')
		->setCategory('Test result file');

		$object->setActiveSheetIndex(0);
		$object->getActiveSheet()->setCellValue('A1', 'No');
		$object->getActiveSheet()->setCellValue('B1', 'Nama Obat');
		$object->getActiveSheet()->setCellValue('C1', 'Satuan');
		$object->getActiveSheet()->setCellValue('D1', 'Harga');
		$object->getActiveSheet()->setCellValue('E1', 'Stok');
		$object->getActiveSheet()->setCellValue('F1', 'Jumlah Harga');
		$object->getActiveSheet()->setCellValue('G1', 'Tanggal Expired');

		$baris = 2;
		$no = 1;

		foreach($data['Obat'] as $obat)
		{
			$object->getActiveSheet()->setCellValue('A'.$baris, $no++);
			$object->getActiveSheet()->setCellValue('B'.$baris, $obat['nama_obat']);
			$object->getActiveSheet()->setCellValue('C'.$baris, $obat['satuan']);
			$object->getActiveSheet()->setCellValue('D'.$baris, $obat['harga']);
			$object->getActiveSheet()->setCellValue('E'.$baris, $obat['stok']);
			$object->getActiveSheet()->setCellValue('F'.$baris, $obat['stok']*$obat['harga']);
			$object->getActiveSheet()->setCellValue('G'.$baris, $obat['tanggal_expired']);
			$baris++;
		}

		$object->getActiveSheet()->setTitle("Data Obat");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Data Obat.xls"');
		header('Cache-Control: max-age=0');

		$writer=PHPExcel_IOFactory::createWriter($object, 'Excel2007');
		$writer->save('php://output');

		exit;

	}
}

?>