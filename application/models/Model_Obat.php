<?php

class Model_Obat extends CI_Model{
    
   public function getAllObat()
   {
      return $this->db->query(
         "SELECT 
            a.*, b.nama_jenis_obat
         FROM tb_obat a
         JOIN tb_jenis_obat b ON a.id_jenis_obat = b.id_jenis_obat"

      )->result_array();
   }

   public function hitungJumlahAsset()
{   
    $query = $this->db->get('tb_obat');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
   
}

   

   public function tambahData()
   {
      $config['upload_path']     ='./asset/gambar';
      $config['allowed_types']   ='jpg|png|PNG';
      $config['max_size']        ='10000';
      $config['max_width']       ='5000';
      $config['max_height']      ='5000';

      $this->load->library('upload', $config);
      if (! $this->upload->do_upload('gambar')) 
      {
         echo "Gagal Tambah";
      }else
      {
         $gambar           = $this->upload->data();
         $gambar           = $gambar['file_name'];
         $nama             = $this->input->post('nama_obat', true);
         $id_jenis         = $this->input->post('id_jenis_obat', true);
         $satuan           = $this->input->post('satuan', true);
         $harga            = $this->input->post('harga', true);
         $stok             = $this->input->post('stok', true);
         $tanggal_expired  = $this->input->post('tanggal_expired', true);

         $data = array(
            "nama_obat"       => $nama,
            "id_jenis_obat"   => $id_jenis,
            "satuan"          => $satuan,
            "harga"           => $harga,
            "stok"            => $stok,
            "tanggal_expired" => $tanggal_expired,
            "gambar"          => $gambar,
         );
         $this->db->insert('tb_obat', $data);
      }
   }

   public function getObatById($id_obat)
   {
      return $this->db->get_where('tb_obat', ['id_obat' => $id_obat])->row_array();
   }

   public function update()
   {
      $config['upload_path']     ='./asset/gambar';
      $config['allowed_types']   ='jpg|png|PNG';
      $config['max_size']        ='10000';
      $config['max_width']       ='5000';
      $config['max_height']      ='5000';

      $this->load->library('upload', $config);
      if (! $this->upload->do_upload('gambar')) 
      {
         $data = [
            "nama_obat" => $this->input->post('nama_obat', true),
            "id_jenis_obat" => $this->input->post('id_jenis_obat', true),
            "satuan" => $this->input->post('satuan', true),
            "harga" => $this->input->post('harga', true),
            "stok" => $this->input->post('stok', true),
            "gambar" => $this->input->post('altgambar', true),
            "tanggal_expired" => $this->input->post('tanggal_expired', true),
         ];
         $this->db->where('id_obat', $this->input->post('id_obat'));
         $this->db->update('tb_obat', $data);
      }else
      {
         $gambar           = $this->upload->data();
         $gambar           = $gambar['file_name'];
         $data = [
            "nama_obat" => $this->input->post('nama_obat', true),
            "id_jenis_obat" => $this->input->post('id_jenis_obat', true),
            "satuan" => $this->input->post('satuan', true),
            "harga" => $this->input->post('harga', true),
            "stok" => $this->input->post('stok', true),
            "gambar" => $gambar,
            "tanggal_expired" => $this->input->post('tanggal_expired', true),
         ];
         $this->db->where('id_obat', $this->input->post('id_obat'));
         $this->db->update('tb_obat', $data);
      }
   }

   public function hapusData($id_obat)
   {
      $this->db->where('id_obat', $id_obat);
      $this->db->delete('tb_obat');
   }

   public function get_keyword($keyword)
   {
      $this->db->select('*');
      $this->db->from('tb_obat');
      $this->db->like('nama_obat', $keyword);
      return $this->db->get()->result_array();
   }

   public function get_jenis($keyword)
   {
      $this->db->select('*');
      $this->db->from('tb_obat');
      $this->db->like('id_jenis_obat', $keyword);
      return $this->db->get()->result_array();
   }

   public function rentang($tawal, $takhir)
   {
      return $this->db->query(
         "SELECT * FROM tb_obat 
         WHERE tanggal_expired 
         BETWEEN '$tawal' AND '$takhir' ")->result_array();
     
   }
}  