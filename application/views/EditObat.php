
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Edit Data Obat</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div>
      <?= form_open_multipart('Obat/editData/'); ?>  
      <input type="hidden" name="id_obat"  value="<?= $Obat['id_obat']; ?>"> 
  <div class="mb-3">
    <label for="username" class="form-label">Nama Obat</label>
    <input type="text" class="form-control" id="nama" name="nama_obat" value="<?= $Obat['nama_obat']; ?>">
    <label for="username" class="form-label">Jenis Obat</label>
    <!-- <input type="text" class="form-control" id="nama" name="id_jenis_obat" value="<?= $Obat['id_jenis_obat']; ?>" disabled readonly> -->
    <select class="form-control form-select-lg mb-3" aria-label=".form-select-lg example" name="id_jenis_obat">
        <option selected>-- Pilih Jenis Obat -- </option>
        <?php foreach ($JenisObat as $item): ?>
        <option value="<?= $item['id_jenis_obat']; ?>" <?= ($item['id_jenis_obat'] == $Obat['id_jenis_obat'])?"selected":'' ?>><?= $item['nama_jenis_obat']; ?></option>
       <?php endforeach;?>
    </select>
    <label for="username" class="form-label">Satuan</label>
    <input type="text" class="form-control" id="nama" name="satuan" value="<?= $Obat['satuan']; ?>">
    <label for="username" class="form-label">Harga</label>
    <input type="text" class="form-control" id="nama" name="harga" value="<?= $Obat['harga']; ?>">
    <label for="username" class="form-label">Jumlah Stok</label>
    <input type="text" class="form-control" id="nama" name="stok" value="<?= $Obat['stok']; ?>">
    <label for="username" class="form-label">Tanggal Expired</label>
    <input type="date" class="form-control" id="nama" name="tanggal_expired" value="<?= $Obat['tanggal_expired']; ?>">
    <label for="formFile" class="form-label">Gambar</label>
    <input class="form-control" type="file" id="formFile" name="gambar" >
    <input type="hidden" class="form-control" id="nama" name="altgambar" value="<?= $Obat['gambar']; ?>">
  <button type="submit" class="btn btn-primary mt-4">Simpan</button>
  <?= form_close() ?>

       
      </div>
    </section>
  </div>
